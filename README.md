#FontScroll

jquery插件开发练习，一个文字行向上滚动插件。

#最新更新：20140708

重构代码，更加规范。

#最新更新：20140613

修正行内不能加padding,margin,border的bug。定义样式确保外围div是li的height+padding+margin+border的总高的倍数。
提示：外围div不能存在padding值，不然li的border就会出现跳动bug。

#最新更新：20140611

增加自定义行的添加样式，比如五行滚动中第三行显示着色，在调用的时候加上{num: 2}就可以了。num零代表第一行，依次递增，默认是第二行。即num: 1。


#使用简介

思路是利用margin-top的负值来实现滚动，可以自定义滚动间隔和文字行上class样式定义。项目开发中需求是显示三行，间隔滚动一行，中间的添加样式，暂时只支持了第二行的样式添加。

css定义注意事项：外div的高度需要是行的倍数或等高。
js调用：
$(function(){
				$('#FontScroll').FontScroll({Time: 3000,Num: 1});
});